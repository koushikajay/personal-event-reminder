import boto3
from helper import Helper
from datetime import datetime
from dateutil.relativedelta import relativedelta


# TODO : Under the assumption that it might take upto 48 hours for dynamodb stream to trigger
class Events(object):
    def __init__(self, event):
        self.event_owner_name = event['userName']
        self.event_type = event['eventName']
        self.event_date = event['eventDate']
        self.event_owner_contact = event['phoneNumber']
        # self.event_owner_country = event['eventOwnerCountry']
        self.event_owner_email_address = event['emailAddress']
        self.event_expiry_datetime = event['eventReminderDate']
        self.validated_events = ["BIRTHDAY", "ANNIVERSARY"]

    def get_event_wishes(self):
        event_wishes = {"birthday": "Happy Birthday, Have a Great Day!",
                        "anniversary": "Wish you a Happy Anniversary. Enjoy!!"}
        return event_wishes[self.event_type.lower()]

    # TODO:Modiy SNS Topic name dynamically
    def send_notification(self):
        client = boto3.client('sns')
        try:
            response = client.publish(
                TopicArn='arn:aws:sns:us-east-1:291299730216:dynamodb',
                Message=self.get_event_wishes(),
                Subject='{} Event Reminder'.format(self.event_type)

            )

            print(response)
            print("Notification Success!!")
        except Exception as e:
            print("Notification Failed !!")
            raise e

    def birthday_event(self):
        pass

    def anniversary_event(self):
        pass

    def validate_event(self):
        print("Validating  details of the Event...")
        if not self.is_valid_event_type():
            print("Not an Event Currently Supported. Please Check in back Later for the support")
            exit(1)
        print("Valid Event Type {}".format(self.event_type))
        return True

    def is_valid_event_type(self):
        if self.event_type.upper() in self.validated_events:
            return True
        return False

    def is_valid_phone_number(self):
        # TODO: Implement Validation of Phone Number
        print("Is Contact Number {} Valid?".format(self.event_owner_contact))
        return True

    def is_valid_email_address(self):
        # TODO: Implement Validation of Email Address
        print("Is Email Address {} Valid?".format(self.event_owner_email_address))
        return True

    def calculate_event_new_expiry(self):
        current_event_reminder_date = datetime.fromtimestamp(self.event_expiry_datetime)
        next_event_reminder_date = current_event_reminder_date + relativedelta(years=1)
        next_event_reminder_epoch = int((next_event_reminder_date - datetime(1970, 1, 1)).total_seconds())
        return next_event_reminder_epoch


def event_notifier(event, context):
    dynamodb_helper = Helper()
    for triggered_event in event['Records']:

        try:
            # Check if the event is Delete
            if triggered_event['eventName'] == 'REMOVE':
                print("Event:{}".format(triggered_event))
                print("Event Action:{}".format(triggered_event['eventName']))
                print("Event Source ARN:{}".format(triggered_event['eventSourceARN']))
                print("Event Keys:{}".format(triggered_event['dynamodb']['Keys']))
                event_metadata = triggered_event['dynamodb']['OldImage']
                event_metadata = dynamodb_helper.unmarshal_dynamodb_json(event_metadata)
                print("Event Details : {}".format(event_metadata))
                event_instance = Events(event_metadata)
                if not event_instance.validate_event():
                    print("Not a Valid Event . Please check the Event")
                    exit(1)
                print('Valid Event ... Proceeding further to notification')
                event_instance.__getattribute__('{}_event'.format(event_instance.event_type.lower()))()
                event_instance.send_notification()
                event_new_expiry_time = event_instance.calculate_event_new_expiry()
                # dt_obj = datetime.strptime(datetime.strftime(event_new_expiry_time, '%Y-%m-%d'), '%Y-%m-%d')
                # seconds = int(dt_obj.timestamp())
                # event_instance.event_expiry_datetime = seconds
                event_instance.event_expiry_datetime = event_new_expiry_time
                print(event_instance.event_date)
                print(event_instance.event_expiry_datetime)
                print({
                    'userName': event_instance.event_owner_name,
                    'eventName': event_instance.event_type,
                    'eventDate': event_instance.event_date,
                    'eventReminderDate': event_instance.event_expiry_datetime,
                    'phoneNumber': event_instance.event_owner_contact,
                    'emailAddress': event_instance.event_owner_email_address,
                    'relationship': 'Self'

                })
                dynamodb_helper.put_item_dynamodb_table(event_instance)
        except Exception as e:
            raise e
