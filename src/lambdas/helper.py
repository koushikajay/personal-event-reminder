import boto3


class Helper(object):
    @staticmethod
    def initiate_dynamodb_client():
        dynamodb = boto3.resource('dynamodb')
        return dynamodb

    @staticmethod
    def put_item_dynamodb_table(event):
        #TODO: Modify the Relationship
        db = Helper.initiate_dynamodb_client()
        try:
            table = db.Table('personal_events_metadata')
            response = table.put_item(
                Item={
                    'userName': event.event_owner_name,
                    'eventName': event.event_type,
                    'eventDate': event.event_date,
                    'eventReminderDate': event.event_expiry_datetime,
                    'phoneNumber': event.event_owner_contact,
                    'emailAddress': event.event_owner_email_address,
                    'relationship': 'Self'

                })
            print(response)
        except Exception as e:
            raise e

    def _unmarshal_value(self, db_item):
        if type(db_item) is not dict:
            return db_item
        for key, value in db_item.items():
            if key == 'BOOL':
                return value
            if key == 'NULL':
                return None
            if key == 'S':
                return value
            if key == 'N':
                if '.' in str(value):
                    return float(value)
                return int(value)
            # if key == 'L':
            #     data = []
            #     for item in value:
            #         data.append(self._unmarshal_value(item))
            #     return data
            if key == 'M':
                data = {}
                for k1, v1 in value.items():
                    if k1 == 'L':
                        data = [self._unmarshal_value(n) for n in v1]
                    else:

                        data[k1] = self._unmarshal_value(v1)
                return data

    def unmarshal_dynamodb_json(self, item):
        data = dict({})
        data['M'] = item
        return self._unmarshal_value(data)
