
resource "aws_api_gateway_rest_api" "PersonalEventReminderAPI" {
  name = "PersonalEventReminderAPI"
  description = "API to record all personal events to DynamoDB table"
//  endpoint_configuration {
//    types = ["REGIONAL"]
//  }
}

resource "aws_api_gateway_resource" "PersonalEventReminderResource" {
  parent_id = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.root_resource_id}"
  path_part = "events"
  rest_api_id = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
}

resource "aws_api_gateway_method" "PersonalEventReminderMethod" {
  authorization = "NONE"
  http_method = "POST"
  resource_id = "${aws_api_gateway_resource.PersonalEventReminderResource.id}"
  rest_api_id = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
}

resource "aws_api_gateway_integration" "PersonalEventReminderIntegration" {
  http_method = "POST"
  resource_id = "${aws_api_gateway_resource.PersonalEventReminderResource.id}"
  rest_api_id = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
  type = "AWS"
  integration_http_method = "POST"
  uri = "arn:aws:apigateway:us-east-1:dynamodb:action/PutItem"
  credentials = "arn:aws:iam::291299730216:role/service-role/EC2Instances-role-38ldx7qa"
  request_templates = {
    "application/json" = <<REQUEST_TEMPLATE
  {
    "TableName": "${var.table_name}",
    "Item":{
        "userName": {
          "S": "$input.path('$.userName')"
        },
        "eventName": {
          "S": "$input.path('$.eventName')"
        },
        "eventDate": {
          "S": "$input.path('$.eventDate')"
        },
        "eventReminderDate": {
          "N": "$input.path('$.eventReminderDate')"
        },
        "emailAddress": {
          "S": "$input.path('$.emailAddress')"
        },
        "phoneNumber": {
          "S": "$input.path('$.phoneNumber')"
        },
        "relationship": {
          "S": "$input.path('$.relationship')"
        }
    }
  }

  REQUEST_TEMPLATE
  }
  passthrough_behavior = "WHEN_NO_MATCH"
}
//resource "aws_api_gateway_stage" "PersonalEventReminderStage" {
//  stage_name    = "dev"
//  rest_api_id   = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
//  deployment_id = "${aws_api_gateway_deployment.PersonalEventReminderDeployment.id}"
//}


resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
  resource_id = "${aws_api_gateway_resource.PersonalEventReminderResource.id}"
  http_method = "${aws_api_gateway_method.PersonalEventReminderMethod.http_method}"
  status_code = "200"
  response_parameters ={
    "method.response.header.Access-Control-Allow-Origin" = true
  }
}

resource "aws_api_gateway_integration_response" "PersonalEventReminderIntegrationResponse" {
  rest_api_id = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
  resource_id = "${aws_api_gateway_resource.PersonalEventReminderResource.id}"
  http_method = "${aws_api_gateway_method.PersonalEventReminderMethod.http_method}"
  status_code = "${aws_api_gateway_method_response.response_200.status_code}"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
}
resource "aws_api_gateway_method" "options_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
  resource_id   = "${aws_api_gateway_resource.PersonalEventReminderResource.id}"
  http_method   = "OPTIONS"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "options_integration" {
  rest_api_id   = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
  resource_id   = "${aws_api_gateway_resource.PersonalEventReminderResource.id}"
  http_method   = "${aws_api_gateway_method.options_method.http_method}"
  type          = "MOCK"
  request_templates = {
    "application/json" = "{ \"statusCode\": 200 }"
  }
}
resource "aws_api_gateway_integration_response" "options_integration_response" {
  rest_api_id   = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
  resource_id   = "${aws_api_gateway_resource.PersonalEventReminderResource.id}"
  http_method   = "${aws_api_gateway_method.options_method.http_method}"
  status_code   = "${aws_api_gateway_method_response.options_200.status_code}"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
    "method.response.header.Access-Control-Allow-Methods" = "'GET,OPTIONS,POST,PUT'",
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
  depends_on = ["aws_api_gateway_integration.options_integration","aws_api_gateway_method_response.options_200"]
}

resource "aws_api_gateway_method_response" "options_200" {
  rest_api_id   = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
  resource_id   = "${aws_api_gateway_resource.PersonalEventReminderResource.id}"
  http_method   = "${aws_api_gateway_method.options_method.http_method}"
  status_code   = 200
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters ={
    "method.response.header.Access-Control-Allow-Headers" = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Origin" = true
  }
  depends_on = ["aws_api_gateway_method.options_method"]
}

resource "aws_api_gateway_deployment" "PersonalEventReminderDeployment" {
  depends_on = ["aws_api_gateway_integration.PersonalEventReminderIntegration","aws_api_gateway_integration.options_integration"]
  rest_api_id = "${aws_api_gateway_rest_api.PersonalEventReminderAPI.id}"
  stage_name  = "dev"

}
