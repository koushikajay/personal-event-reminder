variable "table_name" {}
variable "allow_headers" {
  type = "list"
  default = ["Content-Type", "X-Amz-Date", "Authorization", "X-Api-Key", "X-Amz-Security-Token", "X-Requested-With"]
}

variable "allow_methods" {
  type = "list"
  default = ["*"]
}

variable "allow_origin" {
  default = "*"
}
