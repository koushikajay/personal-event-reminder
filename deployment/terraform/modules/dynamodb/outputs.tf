output "dynamo_stream_arn" {
  value = "${aws_dynamodb_table.personal-event-reminder-dynamodb-table.stream_arn}"
}
output "dynamodb_table_name" {
  value = "${aws_dynamodb_table.personal-event-reminder-dynamodb-table.name}"
}