variable "aws_region_name" { default = "us-east-1" }
variable "table_name" {}
variable "billing_mode" {}
variable "read_capacity" {}
variable "write_capacity" {}
variable "hash_key" {}
variable "range_key" {}
variable "ttl" {}
variable "environment" {default = "dev"}
