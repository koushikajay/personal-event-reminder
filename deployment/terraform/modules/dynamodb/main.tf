resource "aws_dynamodb_table" "personal-event-reminder-dynamodb-table" {
  name           = "${var.table_name}"
  billing_mode   = "${var.billing_mode}"
  read_capacity  = "${var.read_capacity}"
  write_capacity = "${var.write_capacity}"
  hash_key       = "${var.hash_key}"
  range_key      = "${var.range_key}"

  attribute {
    name = "${var.hash_key}"
    type = "S"
  }

  attribute {
    name = "${var.range_key}"
    type = "S"
  }


  ttl {
    attribute_name = "${var.ttl}"
    enabled        = true
  }
  stream_enabled = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  tags = {
    Name        = "${var.table_name}"
    Environment = "${var.environment}"
  }
}
