output "s3_bucket" {
  value = "${aws_s3_bucket.personal_event_reminder_bucket.bucket}"
}

output "s3_endpoint" {
  value = "${aws_s3_bucket.personal_event_reminder_bucket.website_endpoint}"
}