provider "aws" {
  region = "${var.aws_region_name}"
}
resource "aws_s3_bucket" "personal_event_reminder_bucket" {
  bucket = "${var.bucket_name}"
  acl    = "${var.acl}"
  website {
    index_document = "index.html"
  }
}