data "archive_file" "lambda_zip" {
  type        = "zip"
  source_dir  = "../../../../src/lambdas"
  output_path = "${var.s3_key}"
}
resource "aws_lambda_function" "personal_event_reminder_lambda_function" {
  filename = "${var.s3_key}"
  function_name = "${var.function_name}"
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
//  s3_bucket = "${var.bucket_name}"
//  s3_key = "${var.s3_key}"
  handler = "${var.handler}"
  role = "${var.role}"
  runtime = "${var.runtime}"
}


resource "aws_lambda_event_source_mapping" "example" {
  event_source_arn  = "${var.event_source_arn}"
  function_name     = "${aws_lambda_function.personal_event_reminder_lambda_function.arn}"
  starting_position = "LATEST"
}