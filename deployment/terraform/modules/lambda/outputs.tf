output "aws_lambda_function_arn" {
  value = "${aws_lambda_function.personal_event_reminder_lambda_function.arn}"
}