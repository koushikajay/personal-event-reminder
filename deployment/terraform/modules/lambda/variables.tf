variable "event_source_arn" {}
variable "function_name" {}
variable "bucket_name" {}
variable "s3_key" {}
variable "handler" {}
variable "runtime" {}
variable "role" {}