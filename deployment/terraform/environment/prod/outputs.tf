output "final_s3_bucket" {
  value = "${module.per_s3_bucket.s3_bucket}"
}

output "final_dynamodb_table" {
  value = "${module.per_dynamodb_table.dynamodb_table_name}"
}

output "final_dynamodb_stream_arn" {
  value = "${module.per_dynamodb_table.dynamo_stream_arn}"
}

output "final_s3_static_endpoint" {
  value = "${module.per_s3_bucket.s3_endpoint}"
}

output "final_lambda_function" {
  value = "${module.per_lambda.aws_lambda_function_arn}"
}
