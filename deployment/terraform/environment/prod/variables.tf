variable "aws_region_name" { default = "us-east-1" }
variable "table_name" {}
variable "billing_mode" {}
variable "read_capacity" {}
variable "write_capacity" {}
variable "hash_key" {}
variable "range_key" {}
variable "ttl" {}
variable "environment" {default = "dev"}
variable "acl" { default = "private" }
variable "bucket_name" {}
//variable "lambda_function_name" {}
//variable "event_source_arn" {}
variable "function_name" {}
variable "s3_key" {}
variable "handler" {}
variable "runtime" {}
variable "role" {}
//variable "stream_arn" {}
