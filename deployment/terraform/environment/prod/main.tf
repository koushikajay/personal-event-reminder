module "per_s3_bucket" {
  source = "../../modules/s3"
  bucket_name = "${var.bucket_name}"
}

module "per_dynamodb_table" {
  source = "../../modules/dynamodb"
  write_capacity = "${var.write_capacity}"
  ttl = "${var.ttl}"
  table_name = "${var.table_name}"
  read_capacity = "${var.read_capacity}"
  range_key = "${var.range_key}"
  hash_key = "${var.hash_key}"
  billing_mode = "${var.billing_mode}"
}

module "per_lambda" {
  source = "../../modules/lambda"
  event_source_arn = "${module.per_dynamodb_table.dynamo_stream_arn}"
  function_name = "${var.function_name}"
  s3_key="${var.s3_key}"
  bucket_name = "${var.bucket_name}"
  handler = "${var.handler}"
  role = "${var.role}"
  runtime = "${var.runtime}"
}

module "per_api" {
  source = "../../modules/apigateway"
  table_name = "${var.table_name}"
}

