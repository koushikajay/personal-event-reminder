# Personal Event Reminder

A simple personal event reminder application through email or slack

## Components
    1.S3 Bucket
    2.API Gateway
    3.DynamoDB
    4.Lambda
    5.Cloudwatch
    
## Architecture

![alt text](static/images/Design_PersonalEventReminderApp.png "Architecture")
